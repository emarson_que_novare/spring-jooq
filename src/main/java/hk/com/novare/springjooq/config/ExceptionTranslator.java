package hk.com.novare.springjooq.config;

import org.jooq.ExecuteContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultExecuteListener;
import org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator;
import org.springframework.jdbc.support.SQLExceptionTranslator;

public class ExceptionTranslator extends DefaultExecuteListener {
    public final void exception(final ExecuteContext context) {
        final SQLDialect dialect = context.configuration().dialect();
        final SQLExceptionTranslator translator = new SQLErrorCodeSQLExceptionTranslator(dialect.name());
        context.exception(
            translator.translate(
                "Access database using jOOQ",
                context.sql(),
                context.sqlException()
            )
        );
    }
}
