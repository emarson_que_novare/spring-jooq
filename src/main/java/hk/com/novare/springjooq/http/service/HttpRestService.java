package hk.com.novare.springjooq.http.service;

import io.reactivex.Flowable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

public interface HttpRestService {
    <T> Flowable<T> exchangeForBody(RequestEntity<T> requestEntity, Class<T> clazz);

    <T> Flowable<T> exchangeForBody(RequestEntity<T> requestEntity, ParameterizedTypeReference<T> typeRef);

    <T> Flowable<ResponseEntity<T>> exchange(RequestEntity<T> requestEntity, Class<T> clazz);

    <T> Flowable<ResponseEntity<T>> exchange(RequestEntity<T> requestEntity, ParameterizedTypeReference<T> typeRef);
}
