package hk.com.novare.springjooq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class MainApp {
    public MainApp() { }

    public static void main(final String[] args) {
        SpringApplication.run(MainApp.class, args);
    }
}
