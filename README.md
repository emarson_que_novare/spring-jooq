# Spring JOOQ

[![pipeline status](https://gitlab.com/jed_cua/spring-jooq/badges/develop/pipeline.svg)](https://gitlab.com/jed_cua/spring-jooq/commits/develop)
[![coverage report](https://gitlab.com/jed_cua/spring-jooq/badges/develop/coverage.svg)](https://gitlab.com/jed_cua/spring-jooq/commits/develop)

Scaffolding for Spring Microservices

### Features
* [Spring Boot](https://spring.io/projects/spring-boot/)
* [JOOQ](https://www.jooq.org/)
* [RxJava](https://github.com/ReactiveX/RxJava/)
* [Liquibase](http://www.liquibase.org/)
* [Checkstyle](http://checkstyle.sourceforge.net/)
* [Spotbugs](https://spotbugs.github.io/)
* [PMD](https://pmd.github.io/)
* [ArchUnit](https://www.archunit.org/)
* [Hamcrest](http://hamcrest.org/JavaHamcrest/)