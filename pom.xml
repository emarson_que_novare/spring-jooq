<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>hk.com.novare</groupId>
    <artifactId>spring-jooq</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>jar</packaging>

    <name>spring-jooq</name>
    <description>Spring + Jooq </description>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.0.3.RELEASE</version>
        <!-- lookup parent from repository -->
        <relativePath/>
    </parent>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>

        <!-- Base directory and package name for generated JOOQ code -->
        <jooq.generated.target.directory>src/main/java</jooq.generated.target.directory>
        <jooq.generated.target.package>jooq</jooq.generated.target.package>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-jooq</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>io.reactivex.rxjava2</groupId>
            <artifactId>rxjava</artifactId>
            <version>2.2.2</version>
        </dependency>

        <dependency>
            <groupId>org.liquibase</groupId>
            <artifactId>liquibase-core</artifactId>
            <version>3.6.2</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.46</version>
        </dependency>

        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <version>9.4-1206-jdbc42</version>
        </dependency>

        <dependency>
            <groupId>org.yaml</groupId>
            <artifactId>snakeyaml</artifactId>
            <version>1.23</version>
        </dependency>

        <dependency>
            <groupId>org.jooq</groupId>
            <artifactId>jooq</artifactId>
            <version>3.10.7</version>
        </dependency>

        <dependency>
            <groupId>org.jooq</groupId>
            <artifactId>jooq-meta</artifactId>
            <version>3.10.7</version>
        </dependency>

        <dependency>
            <groupId>com.tngtech.archunit</groupId>
            <artifactId>archunit</artifactId>
            <version>0.9.1</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.github.spotbugs</groupId>
            <artifactId>spotbugs</artifactId>
            <version>3.1.3</version>
        </dependency>

        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-all</artifactId>
            <version>1.3</version>
            <scope>test</scope>
        </dependency>

        <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-config</artifactId>
        </dependency>
    </dependencies>

    <profiles>
        <profile>
            <id>dev</id>
            <properties>
                <activatedProperties>dev</activatedProperties>
            </properties>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
        </profile>
        <profile>
            <id>cicd</id>
            <properties>
                <activatedProperties>cicd</activatedProperties>
            </properties>
        </profile>
    </profiles>

    <dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>Finchley.SR1</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>

            <!-- Reads external properties file -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>properties-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>initialize</phase>
                        <goals>
                            <goal>read-project-properties</goal>
                        </goals>
                        <configuration>
                            <urls>
                                <url>http://10.23.23.55:8888/spring-jooq.properties</url>
                                <url>http://10.23.23.55:8888/spring-jooq-${activatedProperties}.properties</url>
                            </urls>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- Executes Liquibase migrations -->
            <plugin>
                <groupId>org.liquibase</groupId>
                <artifactId>liquibase-maven-plugin</artifactId>
                <version>3.6.2</version>
                <configuration>
                    <changeLogFile>src/main/resources/liquibase/db.changelog-master.yaml</changeLogFile>
                    <driver>${spring.datasource.driver-class-name}</driver>
                    <url>${spring.datasource.url}</url>
                    <username>${spring.datasource.username}</username>
                    <password>${spring.datasource.password}</password>
                    <verbose>true</verbose>
                    <promptOnNonLocalDatabase>false</promptOnNonLocalDatabase>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>org.yaml</groupId>
                        <artifactId>snakeyaml</artifactId>
                        <version>1.23</version>
                    </dependency>
                </dependencies>
            </plugin>

            <!-- Generates JOOQ code from database -->
            <plugin>
                <groupId>org.jooq</groupId>
                <artifactId>jooq-codegen-maven</artifactId>
                <version>3.10.7</version>
                <configuration>
                    <jdbc>
                        <driver>${spring.datasource.driver-class-name}</driver>
                        <url>${spring.datasource.url}</url>
                        <user>${spring.datasource.username}</user>
                        <password>${spring.datasource.password}</password>
                    </jdbc>
                    <generator>
                        <generate>
                            <pojos>true</pojos>
                        </generate>
                        <database>
                            <name>${jooq.codegen.database}</name>
                            <inputSchema>${jooq.codegen.schema}</inputSchema>
                            <outputSchemaToDefault>true</outputSchemaToDefault>
                        </database>
                        <target>
                            <packageName>${jooq.generated.target.package}</packageName>
                            <directory>${jooq.generated.target.directory}</directory>
                        </target>
                    </generator>
                </configuration>
            </plugin>

            <!-- Static Analysis Plugins -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>3.0.0</version>
                <executions>
                    <execution>
                        <id>checkstyle</id>
                        <phase>package</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <excludes>**/${jooq.generated.target.package}/**/*</excludes>
                    <suppressionsLocation>static_analysis/checkstyle_supression.xml</suppressionsLocation>
                    <configLocation>static_analysis/checkstyle.xml</configLocation>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>com.puppycrawl.tools</groupId>
                        <artifactId>checkstyle</artifactId>
                        <version>8.12</version>
                    </dependency>
                </dependencies>
            </plugin>
            <plugin>
                <groupId>com.github.spotbugs</groupId>
                <artifactId>spotbugs-maven-plugin</artifactId>
                <version>3.1.3</version>
                <executions>
                    <execution>
                        <id>spotbugs</id>
                        <phase>package</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <effort>Max</effort>
                    <excludeFilterFile>static_analysis/spotbugs_exclude.xml</excludeFilterFile>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-pmd-plugin</artifactId>
                <version>3.9.0</version>
                <configuration>
                    <printFailingErrors>true</printFailingErrors>
                    <verbose>true</verbose>
                    <minimumPriority>1</minimumPriority>
                    <rulesets>
                        <ruleset>/static_analysis/pmd.xml</ruleset>
                    </rulesets>
                    <excludes>
                        <excludes>**/${jooq.generated.target.package}/**/*</excludes>
                    </excludes>
                </configuration>
                <executions>
                    <execution>
                        <id>pmd</id>
                        <phase>package</phase>
                        <goals>
                            <goal>check</goal>
                            <goal>cpd-check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- Code Coverage Report -->
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.2</version>
                <executions>
                    <execution>
                        <id>pre-unit-test</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>post-unit-test</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <excludes>**/${jooq.generated.target.package}/**/*</excludes>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
